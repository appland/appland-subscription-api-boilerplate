'use strict';

const crypto = require('crypto');
const rp = require('request-promise-native');
const querystring = require('querystring');

Object.assign(String.prototype, {
	toUrlSafeBase64() {
		return this
			.split("/").join("_")
			.split("+").join("-")
			.split("=").join("");
	}
});

/**
 * ApplandSubscriptionAPIPushEvent sends subscription events to ApplandSubsctiptionAPI
 */
class ApplandSubscriptionAPIPushEvent {

	/**
	 * @param {string} endpoint
	 * @param {{key: string, secret: string, timeout: number}} credentials Credentials for this signature.
	 */
	constructor(endpoint, credentials) {
		this.endpoint = endpoint;
		this.credentials = credentials;
	}

	/**
	 * Create request signature
	 * @param {{key: string, secret: string, timeout: number}[]} credentialsSet Credentials for this signature.
	 * @param {string} clubId Appland Subscription Club ID
	 * @param {string[]} data The data to be signed.
	 * @param {string} body Optional payload that is included after the timestamp.
	 * @return {Promise<string>} credential key, signature and time
	 */
	createRequestSignature(credentialsSet, clubId, data, body = undefined) {
		const credentials = credentialsSet.find(() => true);
		const key = credentials['key'] || undefined;
		const secret = credentials['secret'] || undefined;
		if (key && secret) {
			const currentTimestamp = Math.trunc(Date.now() / 1000);
			const payload = [].concat(data, [currentTimestamp], body ? [body] : []);
			const hash = crypto
				.createHmac('sha256', secret)
				.update(payload.join('\r\n'))
				.digest('base64')
				.toUrlSafeBase64();
			return Promise.resolve({
				key: key,
				signature: hash,
				time: currentTimestamp
			})
		} else {
			return Promise.reject(new Exception('Missing key or secret'));
		}
	}

	/**
	 * Sends a raw event to ApplandSubsctiptionAPI
	 * @param {string} subscription
	 * @param {string} payload
	 * @returns {Promise<{} | {message: {string}, statusCode: {int}}>}
	 */
	sendRawEvent(subscription, payload) {
		const httpMethod = 'POST';
		return this.createRequestSignature([this.credentials], subscription, [httpMethod, subscription], payload)
			.then((signature) => {
				const uri = `${this.endpoint}/api/subscription/events/${subscription}?${querystring.stringify(signature)}`;
				console.log('End point: ' + uri);
				return rp({
					method: httpMethod,
					uri: uri,
					body: payload,
				})
			})
			.then((result) => {
				return JSON.parse(result);
			})
			.catch((err) => {
				return {
					statusCode: err.statusCode,
					message: (JSON.parse(err.error || '{}') || {}).message,
				};
			});
	}

	/**
	 * Sends an event to ApplandSubsctiptionAPI
	 * @param {string} subscription Subscription name
	 * @param {{}} options Options for the request
	 * @option {boolean} isEligible True if the user is eligible to use the service at that moment. The last value will override any previously set value independent on the type of event.
	 * @option {string} event Type of event. One of the following values:
	 * 		SUBSCRIBE: User has started a new subscription.
	 * 		BILLED_SUCCESS: User has been billed successfully. Currency and amount shall be included, see below.
	 * 		BILLED_FAILURE: User has been tried to be billed but failed. Currency and amount shall be included, see below.
	 * 		CANCEL: User has manually cancelled the subscription.
	 * 		SUBSCRIPTION_END: Subscription has ended. A subscription can end after too many billed failures or when subscription period ends after the user has cancelled the subscription manually.
	 * @option {string} user User ID of the user in question.
	 * @option {string|null} currency Currency name of the transaction according to ISO 4217. Set to null if event is not BILLED_SUCCESS or BILLED_FAILURE.
	 * @option {float|null} amount The actual amount the user is billed including tax in above currency. Decimal point format (e g 3.14). Set to null if event is not BILLED_SUCCESS or BILLED_FAILURE.
	 * @option {integer|undefined} nextRenewal	Unix Timestamp in seconds that describes when the next scheduled renewal shall occur. This parameter is important to be able to cache the result in the Appland platform. If this is not specified, Appland platform may call partner platform a lot to query for user statuses.
	 * @option {integer|undefined} numberOfProfiles	Defines the number of profiles (e.g. family members) that can be created in this subscription. Default: 1. 1 or missing: Personal account. 2 or more: Family account.
	 * @option {integer|undefined} numberOfConcurrentSessions	Defines the number of concurrent sessions (game plays) per complete user account. Default: 1.
	 * @option {Promise}
	 */
	sendEvent(subscription, options) {
		const payload = JSON.stringify({
			isEligible: options.isEligible,
			event: options.event,
			user: options.user,
			currency: options.currency,
			amount: options.amount,
			nextRenewal: options.nextRenewal,
			numberOfProfiles: options.numberOfProfiles,
			numberOfConcurrentSessions: options.numberOfConcurrentSessions,
		});
		console.log("Post Data: " + payload);
		return this.sendRawEvent(subscription, payload);
	}
}

module.exports = ApplandSubscriptionAPIPushEvent;
