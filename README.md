# Appland Subscription API Boilerplate

This project contains the boilerplate code to send push events to Appland Subscription API.

The following code snippets can be used for your need.

## Mandatory steps to follow
1. Install dependencies by running `npm install`.
2. Now you can make sure that your installation works correctly by running ```npm test``` or ```npm start```. You should see all the test cases passing.
3. Replace the followings to the ones provided by Appland's customer support team.
- {APPLAND_SUBSCRIPTION_API_SUBSCRIPTION_ID}
- {APPLAND_SUBSCRIPTION_API_KEY}
- {APPLAND_SUBSCRIPTION_API_SECRET}

## Instantiate the push event api class
```javascript
// require the only dependency
const ApplandSubscriptionAPIPushEvent = require('./ApplandSubscriptionAPIPushEvent');

// instantiate the push event api class
const subscriptionId = '{APPLAND_SUBSCRIPTION_API_SUBSCRIPTION_ID}';
const pushApi = new ApplandSubscriptionAPIPushEvent(
	'https://api.appland.se',
	{
		key: '{APPLAND_SUBSCRIPTION_API_KEY}',
		secret: '{APPLAND_SUBSCRIPTION_API_SECRET}',
		timeout: 300,
	}
);
```

## Example #1
How to send SUBSCRIBE event:
```javascript
// send SUBSCRIBE event
pushApi.sendEvent(subscriptionId, {
	isEligible: true,
	event: 'SUBSCRIBE',
	user: 'user123',
	currency: null,
	amount: 0,
	nextRenewal: null,
	numberOfProfiles: undefined,
	numberOfConcurrentSessions: undefined,
})
.then((result) => {
	// handle response
	if (Object.keys(result).length === 0) {
		console.log('SUBSCRIBE event successfully sent to Appland Subscription API');
	} else {
		console.log(`Something is wrong: ${result.statusCode} - ${result.message}`);
	}
});
```

## Example #2
How to send SUBSCRIBE event with more details:
```javascript
// send SUBSCRIBE event
pushApi.sendEvent(subscriptionId, {
	isEligible: true,
	event: 'SUBSCRIBE',
	user: 'user123',
	currency: 'EUR',
	amount: 100, // amount in cents
	nextRenewal: null,
	numberOfProfiles: 2,
	numberOfConcurrentSessions: 2,
})
.then((result) => {
	// handle response
	if (Object.keys(result).length === 0) {
		console.log('SUBSCRIBE event successfully sent to Appland Subscription API');
	} else {
		console.log(`Something is wrong: ${result.statusCode} - ${result.message}`);
	}
});
```

## Example #3
How to send SUBSCRIPTION_END event:
```javascript
// send SUBSCRIPTION_END event
pushApi.sendEvent(subscriptionId, {
	isEligible: false,
	event: 'SUBSCRIPTION_END',
	user: 'user123',
	currency: null,
	amount: null,
})
.then((result) => {
	// handle response
	if (Object.keys(result).length === 0) {
		console.log('SUBSCRIPTION_END event successfully sent to Appland Subscription API');
	} else {
		console.log(`Something is wrong: ${result.statusCode} - ${result.message}`);
	}
});
```
