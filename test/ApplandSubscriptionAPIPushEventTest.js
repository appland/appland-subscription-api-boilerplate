'use strict';

const assert = require('assert');

describe('ApplandSubscriptionAPIPushEvent Test', () => {

	const ApplandSubscriptionAPIPushEvent = require('../ApplandSubscriptionAPIPushEvent');

	//TODO: change this with the 'subscriptionId' provided by Appland's customer support team
	const subscriptionId = 'TEST_CLUB';
	const credentials = {
		//TODO: change this with the 'key' provided by Appland's customer support team
		key: 'test-key',
		//TODO: change this with the 'secret' provided by Appland's customer support team
		secret: 'wMXgOk3I8hvEjPJvyUALJFDaaJtV9SeY',
		timeout: 300,
	};
	const endpoint = 'https://api.appland.se';
	const defaultPayload = {
		isEligible: true,
		event: 'SUBSCRIBE',
		user: 'user123',
		currency: null,
		amount: 0,
		nextRenewal: null,
		numberOfProfiles: undefined,
		numberOfConcurrentSessions: undefined,
	};
	const pushApi = new ApplandSubscriptionAPIPushEvent(endpoint, credentials);

	describe('Push events', () => {

		it('Send SUBSCRIBE event', async () => {
			console.log("--------");
			console.log("TEST: Send SUBSCRIBE event");
			const result = await pushApi.sendEvent(subscriptionId, defaultPayload);
			assert.equal(JSON.stringify(result), '{}');
		}).timeout(5000);

		it('Send SUBSCRIBE event with numberOfProfiles and numberOfConcurrentSessions', async () => {
			console.log("--------");
			console.log("TEST: Send SUBSCRIBE event with numberOfProfiles and numberOfConcurrentSessions");
			const payload = {
				...defaultPayload,
				numberOfProfiles: 2,
				numberOfConcurrentSessions: 2,
			};
			const result = await pushApi.sendEvent(subscriptionId, payload);
			assert.equal(JSON.stringify(result), '{}');
		}).timeout(5000);

		it('Send BILLED_SUCCESS with 1 EUR', async () => {
			console.log("--------");
			console.log("TEST: Send BILLED_SUCCESS with 1 EUR");
			const payload = {
				...defaultPayload,
				event: 'BILLED_SUCCESS',
				currency: 'EUR',
				amount: 100, // amount in cents
			};
			const result = await pushApi.sendEvent(subscriptionId, payload);
			assert.equal(JSON.stringify(result), '{}');
		}).timeout(5000);

		it('Send BILLED_FAILURE', async () => {
			console.log("--------");
			console.log("TEST: Send BILLED_FAILURE");
			const payload = {
				...defaultPayload,
				event: 'BILLED_FAILURE',
				isEligible: false,
			};
			const result = await pushApi.sendEvent(subscriptionId, payload);
			assert.equal(JSON.stringify(result), '{}');
		}).timeout(5000);

		it('Send CANCEL', async () => {
			console.log("--------");
			console.log("TEST: Send CANCEL");
			const payload = {
				...defaultPayload,
				event: 'CANCEL',
				isEligible: false,
			};
			const result = await pushApi.sendEvent(subscriptionId, payload);
			assert.equal(JSON.stringify(result), '{}');
		}).timeout(5000);

		it('Send SUBSCRIPTION_END', async () => {
			console.log("--------");
			console.log("TEST: Send SUBSCRIPTION_END");
			const payload = {
				...defaultPayload,
				event: 'SUBSCRIPTION_END',
				isEligible: false,
			};
			const result = await pushApi.sendEvent(subscriptionId, payload);
			assert.equal(JSON.stringify(result), '{}');
		}).timeout(5000);

	}).timeout(30000);

});
